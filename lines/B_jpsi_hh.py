import Functors as F
from GaudiKernel.SystemOfUnits import MeV

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs
from PyConf.Algorithms import Monitor__ParticleRange

# Not a real location... but it looks like it
from Hlt2Conf.lines.sk.b_jpsi_hh_builders import (make_detached_jpsi, make_detached_intermediate_meson, make_parent_B, _JPSI_PDG_MASS, _JPSI_WINDOW)

_PHI_LOWER_MASS = 995. * MeV
_PHI_UPPER_MASS = 1045. * MeV

_KST_LOWER_MASS = 630. * MeV
_KST_UPPER_MASS = 1600. * MeV

all_lines = {}

@register_line_builder(all_lines)
def bs_to_jpsi_phi_line(
        name='Hlt2StarterKit_B0sToJpsiPhi_JpsiToMuMu_PhiToKpKm',
        prescale=1.,
        persistreco=False):
    """
        B0s -> Jpsi (-> mu+ mu-)cc phi (-> K+ K-)cc
    """
    pvs = make_pvs()

    jpsi = make_detached_jpsi(pvs=pvs)
    phi = make_detached_intermediate_meson(
        name='starterkit_detached_intermediate_phi_{hash}',
        pvs=pvs,
        DecayDescriptor="phi(1020) -> K+ K-",
        lower_combination_mass=_PHI_LOWER_MASS - 100 * MeV,
        higher_combination_mass=_PHI_UPPER_MASS + 100 * MeV,
        lower_composite_mass=_PHI_LOWER_MASS,
        higher_composite_mass=_PHI_UPPER_MASS,
        isKaon_p1=True,
        isKaon_p2=True)

    line_alg = make_parent_B(
        intermediate_states=[jpsi, phi],
        DecayDescriptor="B_s0 -> J/psi(1S) phi(1020)")

    jpsi_mass_mon = Monitor__ParticleRange(
        Input=jpsi,
        Variable=F.MASS,
        HistogramName=f"/{name}/jpsi_m",
        Bins=60,
        Range=(_JPSI_PDG_MASS - _JPSI_WINDOW, _JPSI_PDG_MASS + _JPSI_PDG_MASS),
    )
    phi_mass_mon = Monitor__ParticleRange(
        Input=phi,
        Variable=F.MASS,
        HistogramName=f"/{name}/phi_m",
        Bins=60,
        Range=(_PHI_LOWER_MASS, _PHI_UPPER_MASS),
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), jpsi_mass_mon, phi_mass_mon, jpsi, phi, line_alg],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
def bd_to_jpsi_kst_line(
        name='Hlt2StarterKit_B0dToJpsiKst_JpsiToMuMu_KstToKpPim',
        prescale=1.,
        persistreco=False):
    """
        B0d -> Jpsi (-> mu+ mu- )cc K*0 (-> K+ pi-)
    """
    pvs = make_pvs()

    jpsi = make_detached_jpsi(pvs=pvs)
    kst = make_detached_intermediate_meson(
        name='starterkit_detached_intermediate_kstar_{hash}',
        pvs=pvs,
        DecayDescriptor="[K*(892)0 -> K+ pi-]cc",
        lower_combination_mass=_KST_LOWER_MASS - 100 * MeV,
        higher_combination_mass=_KST_UPPER_MASS + 100 * MeV,
        lower_composite_mass=_KST_LOWER_MASS,
        higher_composite_mass=_KST_UPPER_MASS,
        isKaon_p1=True,
        isKaon_p2=False)

    line_alg = make_parent_B(
        intermediate_states=[jpsi, kst],
        DecayDescriptor="B_s0 -> J/psi(1S) K*(892)0")

    jpsi_mass_mon = Monitor__ParticleRange(
        Input=jpsi,
        Variable=F.MASS,
        HistogramName=f"/{name}/jpsi_m",
        Bins=60,
        Range=(_JPSI_PDG_MASS - _JPSI_WINDOW, _JPSI_PDG_MASS + _JPSI_PDG_MASS),
    )
    kst_mass_mon = Monitor__ParticleRange(
        Input=kst,
        Variable=F.MASS,
        HistogramName=f"/{name}/kst_m",
        Bins=60,
        Range=(_PHI_LOWER_MASS, _PHI_UPPER_MASS),
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), jpsi_mass_mon, kst_mass_mon, jpsi, kst, line_alg],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )
