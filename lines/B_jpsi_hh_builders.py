import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm

from Hlt2Conf.standard_particles import make_long_kaons, make_ismuon_long_muon, make_long_pions
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

_JPSI_PDG_MASS = 3096.9 * MeV
_JPSI_WINDOW = 100. * MeV


def make_muons(name='starterkit_detached_muon_{hash}'):
    muons = make_ismuon_long_muon()
    code = F.require_all(F.PT > 500. * MeV, F.CHI2DOF < 5.)

    return ParticleFilter(muons, name=name, Cut=code)


def make_detached_meson(isKaon,
                    pvs,
                    name_template='starterkit_detached_{meson}_{hash}'):
    name = name_template.format(meson="kaon" if isKaon else "pion")
    mesons = make_long_kaons() if isKaon else make_long_pions()
    code = F.require_all(
        F.PT > 120 * MeV,
        F.MINIPCHI2(pvs) > 4.,
    )

    return ParticleFilter(mesons, name=name, Cut=F.FILTER(code))


def make_detached_jpsi(pvs, name='starterkit_detached_jpsi2mumu_{hash}'):
    muons = make_muons()
    combination_code = F.math.in_range(_JPSI_PDG_MASS - _JPSI_WINDOW, F.MASS,
                                       _JPSI_PDG_MASS + _JPSI_WINDOW)

    composite_code = F.require_all(F.PT > 2000. * MeV, F.CHI2DOF < 10.,
                                   F.BPVDLS(pvs) > 3.)

    return ParticleCombiner(
        name=name,
        Inputs=[muons, muons],
        DecayDescriptor='J/psi(1S) -> mu+ mu-',
        CombinationCut=combination_code,
        CompositeCut=composite_code)


def make_detached_intermediate_meson(
        pvs,
        DecayDescriptor,
        lower_combination_mass,
        higher_combination_mass,
        lower_composite_mass,
        higher_composite_mass,
        isKaon_p1,
        isKaon_p2,
        name='starterkit_detached_intermediate_meson_{hash}'):
    meson_p1 = make_detached_meson(isKaon=isKaon_p1, pvs=pvs)
    meson_p2 = make_detached_meson(isKaon=isKaon_p2, pvs=pvs)

    combination_code = F.require_all(
        F.math.in_range(lower_combination_mass, F.MASS,
                        higher_combination_mass), F.MAXDOCACUT(0.2 * mm))

    composite_code = F.require_all(
        F.math.in_rage(lower_composite_mass, F.MASS, higher_composite_mass),
        F.CHI2DOF < 12.)
    return ParticleCombiner([meson_p1, meson_p2],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=composite_code)


def make_parent_B(DecayDescriptor,
                  name='starterkit_parent_bmeson_{hash}',
                  intermediate_states=[]):
    combination_code = F.require_all(
        in_range(4900. * MeV, F.MASS, 6300 * MeV),
        F.SUM(F.PT) > 3000. * MeV,
        F.MAXDOCACUT(0.2 * mm),
    )
    return ParticleCombiner(
        intermediate_states,
        name=name,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code)
