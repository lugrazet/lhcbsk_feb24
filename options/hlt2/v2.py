from Moore import options, run_moore

options.set_input_and_conds_from_testfiledb("exp_24_minbias_Sim10c_magdown")

options.evt_max = 100

options.output_type = 'MDF'
options.output_file = "hlt2_v2_output.mdf"
options.output_manifest_file = "hlt2_v2_output.tck.json"

from Hlt2Conf.lines.qee.dimuon_no_ip import dimuonnoip_massrange3_line


def make_lines():
    return [dimuonnoip_massrange3_line()]


from RecoConf.reconstruction_objects import reconstruction
with reconstruction.bind(from_file=False):
    run_moore(options, make_lines)
