from Moore import options, run_moore

options.set_input_and_conds_from_testfiledb("exp_24_minbias_Sim10c_magdown")

options.evt_max = 100

options.output_type = 'MDF'
options.output_file = "hlt2_v1_output.mdf"
options.output_manifest_file = "hlt2_v1_output.tck.json"

run_moore(options)
