from Moore import options, run_moore

# options.set_input_and_conds_from_testfiledb("exp_24_minbias_Sim10c_magdown")
options.input_files = [
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00001476_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00001481_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00003710_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00003721_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00005093_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00005160_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00005219_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00006206_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00006666_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00006855_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00006995_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00007281_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00007962_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00008009_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00008538_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00009297_1.digi",
    "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00009449_1.digi",
]
options.simulation = True
options.input_type = "ROOT"
options.input_raw_format = 0.5  # https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/different_samples.html#input-samples-with-different-attributes
options.data_type = "Upgrade"
options.dddb_tag = "dddb-20231017"
options.conddb_tag = "sim-20231017-vc-md100"

options.evt_max = 100

options.output_type = 'MDF'
options.output_file = "hlt2_v1_alt_output.mdf"
options.output_manifest_file = "hlt2_v1_alt_output.tck.json"

run_moore(options)
